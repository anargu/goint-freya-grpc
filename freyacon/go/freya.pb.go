// Code generated by protoc-gen-go. DO NOT EDIT.
// source: freyacon/go/freya.proto

package freya

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type PhoneNumber struct {
	CountryCode          string   `protobuf:"bytes,1,opt,name=CountryCode,proto3" json:"CountryCode,omitempty"`
	Number               string   `protobuf:"bytes,2,opt,name=Number,proto3" json:"Number,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PhoneNumber) Reset()         { *m = PhoneNumber{} }
func (m *PhoneNumber) String() string { return proto.CompactTextString(m) }
func (*PhoneNumber) ProtoMessage()    {}
func (*PhoneNumber) Descriptor() ([]byte, []int) {
	return fileDescriptor_9898546896accfc3, []int{0}
}

func (m *PhoneNumber) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PhoneNumber.Unmarshal(m, b)
}
func (m *PhoneNumber) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PhoneNumber.Marshal(b, m, deterministic)
}
func (m *PhoneNumber) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PhoneNumber.Merge(m, src)
}
func (m *PhoneNumber) XXX_Size() int {
	return xxx_messageInfo_PhoneNumber.Size(m)
}
func (m *PhoneNumber) XXX_DiscardUnknown() {
	xxx_messageInfo_PhoneNumber.DiscardUnknown(m)
}

var xxx_messageInfo_PhoneNumber proto.InternalMessageInfo

func (m *PhoneNumber) GetCountryCode() string {
	if m != nil {
		return m.CountryCode
	}
	return ""
}

func (m *PhoneNumber) GetNumber() string {
	if m != nil {
		return m.Number
	}
	return ""
}

type SendSMSParams struct {
	Phone                *PhoneNumber      `protobuf:"bytes,1,opt,name=phone,proto3" json:"phone,omitempty"`
	TemplateName         string            `protobuf:"bytes,3,opt,name=templateName,proto3" json:"templateName,omitempty"`
	Params               map[string]string `protobuf:"bytes,4,rep,name=params,proto3" json:"params,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *SendSMSParams) Reset()         { *m = SendSMSParams{} }
func (m *SendSMSParams) String() string { return proto.CompactTextString(m) }
func (*SendSMSParams) ProtoMessage()    {}
func (*SendSMSParams) Descriptor() ([]byte, []int) {
	return fileDescriptor_9898546896accfc3, []int{1}
}

func (m *SendSMSParams) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SendSMSParams.Unmarshal(m, b)
}
func (m *SendSMSParams) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SendSMSParams.Marshal(b, m, deterministic)
}
func (m *SendSMSParams) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SendSMSParams.Merge(m, src)
}
func (m *SendSMSParams) XXX_Size() int {
	return xxx_messageInfo_SendSMSParams.Size(m)
}
func (m *SendSMSParams) XXX_DiscardUnknown() {
	xxx_messageInfo_SendSMSParams.DiscardUnknown(m)
}

var xxx_messageInfo_SendSMSParams proto.InternalMessageInfo

func (m *SendSMSParams) GetPhone() *PhoneNumber {
	if m != nil {
		return m.Phone
	}
	return nil
}

func (m *SendSMSParams) GetTemplateName() string {
	if m != nil {
		return m.TemplateName
	}
	return ""
}

func (m *SendSMSParams) GetParams() map[string]string {
	if m != nil {
		return m.Params
	}
	return nil
}

type SendSMSResponse struct {
	Sent                 bool     `protobuf:"varint,1,opt,name=sent,proto3" json:"sent,omitempty"`
	Error                *Error   `protobuf:"bytes,2,opt,name=error,proto3" json:"error,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SendSMSResponse) Reset()         { *m = SendSMSResponse{} }
func (m *SendSMSResponse) String() string { return proto.CompactTextString(m) }
func (*SendSMSResponse) ProtoMessage()    {}
func (*SendSMSResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_9898546896accfc3, []int{2}
}

func (m *SendSMSResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SendSMSResponse.Unmarshal(m, b)
}
func (m *SendSMSResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SendSMSResponse.Marshal(b, m, deterministic)
}
func (m *SendSMSResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SendSMSResponse.Merge(m, src)
}
func (m *SendSMSResponse) XXX_Size() int {
	return xxx_messageInfo_SendSMSResponse.Size(m)
}
func (m *SendSMSResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_SendSMSResponse.DiscardUnknown(m)
}

var xxx_messageInfo_SendSMSResponse proto.InternalMessageInfo

func (m *SendSMSResponse) GetSent() bool {
	if m != nil {
		return m.Sent
	}
	return false
}

func (m *SendSMSResponse) GetError() *Error {
	if m != nil {
		return m.Error
	}
	return nil
}

type TemplatesList struct {
	Templates            map[string]*TemplateFields `protobuf:"bytes,1,rep,name=templates,proto3" json:"templates,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	Error                *Error                     `protobuf:"bytes,2,opt,name=error,proto3" json:"error,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                   `json:"-"`
	XXX_unrecognized     []byte                     `json:"-"`
	XXX_sizecache        int32                      `json:"-"`
}

func (m *TemplatesList) Reset()         { *m = TemplatesList{} }
func (m *TemplatesList) String() string { return proto.CompactTextString(m) }
func (*TemplatesList) ProtoMessage()    {}
func (*TemplatesList) Descriptor() ([]byte, []int) {
	return fileDescriptor_9898546896accfc3, []int{3}
}

func (m *TemplatesList) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TemplatesList.Unmarshal(m, b)
}
func (m *TemplatesList) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TemplatesList.Marshal(b, m, deterministic)
}
func (m *TemplatesList) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TemplatesList.Merge(m, src)
}
func (m *TemplatesList) XXX_Size() int {
	return xxx_messageInfo_TemplatesList.Size(m)
}
func (m *TemplatesList) XXX_DiscardUnknown() {
	xxx_messageInfo_TemplatesList.DiscardUnknown(m)
}

var xxx_messageInfo_TemplatesList proto.InternalMessageInfo

func (m *TemplatesList) GetTemplates() map[string]*TemplateFields {
	if m != nil {
		return m.Templates
	}
	return nil
}

func (m *TemplatesList) GetError() *Error {
	if m != nil {
		return m.Error
	}
	return nil
}

type TemplateFields struct {
	Fields               map[string]string `protobuf:"bytes,1,rep,name=fields,proto3" json:"fields,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *TemplateFields) Reset()         { *m = TemplateFields{} }
func (m *TemplateFields) String() string { return proto.CompactTextString(m) }
func (*TemplateFields) ProtoMessage()    {}
func (*TemplateFields) Descriptor() ([]byte, []int) {
	return fileDescriptor_9898546896accfc3, []int{4}
}

func (m *TemplateFields) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TemplateFields.Unmarshal(m, b)
}
func (m *TemplateFields) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TemplateFields.Marshal(b, m, deterministic)
}
func (m *TemplateFields) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TemplateFields.Merge(m, src)
}
func (m *TemplateFields) XXX_Size() int {
	return xxx_messageInfo_TemplateFields.Size(m)
}
func (m *TemplateFields) XXX_DiscardUnknown() {
	xxx_messageInfo_TemplateFields.DiscardUnknown(m)
}

var xxx_messageInfo_TemplateFields proto.InternalMessageInfo

func (m *TemplateFields) GetFields() map[string]string {
	if m != nil {
		return m.Fields
	}
	return nil
}

type SendEmailParams struct {
	To                   map[int32]string  `protobuf:"bytes,1,rep,name=to,proto3" json:"to,omitempty" protobuf_key:"varint,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	Subject              string            `protobuf:"bytes,2,opt,name=subject,proto3" json:"subject,omitempty"`
	TemplateName         string            `protobuf:"bytes,3,opt,name=templateName,proto3" json:"templateName,omitempty"`
	Params               map[string]string `protobuf:"bytes,4,rep,name=params,proto3" json:"params,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *SendEmailParams) Reset()         { *m = SendEmailParams{} }
func (m *SendEmailParams) String() string { return proto.CompactTextString(m) }
func (*SendEmailParams) ProtoMessage()    {}
func (*SendEmailParams) Descriptor() ([]byte, []int) {
	return fileDescriptor_9898546896accfc3, []int{5}
}

func (m *SendEmailParams) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SendEmailParams.Unmarshal(m, b)
}
func (m *SendEmailParams) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SendEmailParams.Marshal(b, m, deterministic)
}
func (m *SendEmailParams) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SendEmailParams.Merge(m, src)
}
func (m *SendEmailParams) XXX_Size() int {
	return xxx_messageInfo_SendEmailParams.Size(m)
}
func (m *SendEmailParams) XXX_DiscardUnknown() {
	xxx_messageInfo_SendEmailParams.DiscardUnknown(m)
}

var xxx_messageInfo_SendEmailParams proto.InternalMessageInfo

func (m *SendEmailParams) GetTo() map[int32]string {
	if m != nil {
		return m.To
	}
	return nil
}

func (m *SendEmailParams) GetSubject() string {
	if m != nil {
		return m.Subject
	}
	return ""
}

func (m *SendEmailParams) GetTemplateName() string {
	if m != nil {
		return m.TemplateName
	}
	return ""
}

func (m *SendEmailParams) GetParams() map[string]string {
	if m != nil {
		return m.Params
	}
	return nil
}

type SendEmailResponse struct {
	Sent                 bool     `protobuf:"varint,1,opt,name=sent,proto3" json:"sent,omitempty"`
	Error                *Error   `protobuf:"bytes,2,opt,name=error,proto3" json:"error,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SendEmailResponse) Reset()         { *m = SendEmailResponse{} }
func (m *SendEmailResponse) String() string { return proto.CompactTextString(m) }
func (*SendEmailResponse) ProtoMessage()    {}
func (*SendEmailResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_9898546896accfc3, []int{6}
}

func (m *SendEmailResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SendEmailResponse.Unmarshal(m, b)
}
func (m *SendEmailResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SendEmailResponse.Marshal(b, m, deterministic)
}
func (m *SendEmailResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SendEmailResponse.Merge(m, src)
}
func (m *SendEmailResponse) XXX_Size() int {
	return xxx_messageInfo_SendEmailResponse.Size(m)
}
func (m *SendEmailResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_SendEmailResponse.DiscardUnknown(m)
}

var xxx_messageInfo_SendEmailResponse proto.InternalMessageInfo

func (m *SendEmailResponse) GetSent() bool {
	if m != nil {
		return m.Sent
	}
	return false
}

func (m *SendEmailResponse) GetError() *Error {
	if m != nil {
		return m.Error
	}
	return nil
}

type TemplateData struct {
	TemplateName         string   `protobuf:"bytes,1,opt,name=templateName,proto3" json:"templateName,omitempty"`
	Data                 []byte   `protobuf:"bytes,2,opt,name=data,proto3" json:"data,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *TemplateData) Reset()         { *m = TemplateData{} }
func (m *TemplateData) String() string { return proto.CompactTextString(m) }
func (*TemplateData) ProtoMessage()    {}
func (*TemplateData) Descriptor() ([]byte, []int) {
	return fileDescriptor_9898546896accfc3, []int{7}
}

func (m *TemplateData) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_TemplateData.Unmarshal(m, b)
}
func (m *TemplateData) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_TemplateData.Marshal(b, m, deterministic)
}
func (m *TemplateData) XXX_Merge(src proto.Message) {
	xxx_messageInfo_TemplateData.Merge(m, src)
}
func (m *TemplateData) XXX_Size() int {
	return xxx_messageInfo_TemplateData.Size(m)
}
func (m *TemplateData) XXX_DiscardUnknown() {
	xxx_messageInfo_TemplateData.DiscardUnknown(m)
}

var xxx_messageInfo_TemplateData proto.InternalMessageInfo

func (m *TemplateData) GetTemplateName() string {
	if m != nil {
		return m.TemplateName
	}
	return ""
}

func (m *TemplateData) GetData() []byte {
	if m != nil {
		return m.Data
	}
	return nil
}

type SaveTemplateResponse struct {
	Saved                bool     `protobuf:"varint,1,opt,name=saved,proto3" json:"saved,omitempty"`
	TemplateName         string   `protobuf:"bytes,2,opt,name=templateName,proto3" json:"templateName,omitempty"`
	Error                *Error   `protobuf:"bytes,3,opt,name=error,proto3" json:"error,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SaveTemplateResponse) Reset()         { *m = SaveTemplateResponse{} }
func (m *SaveTemplateResponse) String() string { return proto.CompactTextString(m) }
func (*SaveTemplateResponse) ProtoMessage()    {}
func (*SaveTemplateResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_9898546896accfc3, []int{8}
}

func (m *SaveTemplateResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SaveTemplateResponse.Unmarshal(m, b)
}
func (m *SaveTemplateResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SaveTemplateResponse.Marshal(b, m, deterministic)
}
func (m *SaveTemplateResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SaveTemplateResponse.Merge(m, src)
}
func (m *SaveTemplateResponse) XXX_Size() int {
	return xxx_messageInfo_SaveTemplateResponse.Size(m)
}
func (m *SaveTemplateResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_SaveTemplateResponse.DiscardUnknown(m)
}

var xxx_messageInfo_SaveTemplateResponse proto.InternalMessageInfo

func (m *SaveTemplateResponse) GetSaved() bool {
	if m != nil {
		return m.Saved
	}
	return false
}

func (m *SaveTemplateResponse) GetTemplateName() string {
	if m != nil {
		return m.TemplateName
	}
	return ""
}

func (m *SaveTemplateResponse) GetError() *Error {
	if m != nil {
		return m.Error
	}
	return nil
}

type UpdateTemplateResponse struct {
	Updated              bool          `protobuf:"varint,1,opt,name=updated,proto3" json:"updated,omitempty"`
	Template             *TemplateData `protobuf:"bytes,2,opt,name=template,proto3" json:"template,omitempty"`
	Error                *Error        `protobuf:"bytes,3,opt,name=error,proto3" json:"error,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *UpdateTemplateResponse) Reset()         { *m = UpdateTemplateResponse{} }
func (m *UpdateTemplateResponse) String() string { return proto.CompactTextString(m) }
func (*UpdateTemplateResponse) ProtoMessage()    {}
func (*UpdateTemplateResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_9898546896accfc3, []int{9}
}

func (m *UpdateTemplateResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateTemplateResponse.Unmarshal(m, b)
}
func (m *UpdateTemplateResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateTemplateResponse.Marshal(b, m, deterministic)
}
func (m *UpdateTemplateResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateTemplateResponse.Merge(m, src)
}
func (m *UpdateTemplateResponse) XXX_Size() int {
	return xxx_messageInfo_UpdateTemplateResponse.Size(m)
}
func (m *UpdateTemplateResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateTemplateResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateTemplateResponse proto.InternalMessageInfo

func (m *UpdateTemplateResponse) GetUpdated() bool {
	if m != nil {
		return m.Updated
	}
	return false
}

func (m *UpdateTemplateResponse) GetTemplate() *TemplateData {
	if m != nil {
		return m.Template
	}
	return nil
}

func (m *UpdateTemplateResponse) GetError() *Error {
	if m != nil {
		return m.Error
	}
	return nil
}

type Error struct {
	ErrorCode            int32    `protobuf:"varint,1,opt,name=errorCode,proto3" json:"errorCode,omitempty"`
	Message              string   `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Error) Reset()         { *m = Error{} }
func (m *Error) String() string { return proto.CompactTextString(m) }
func (*Error) ProtoMessage()    {}
func (*Error) Descriptor() ([]byte, []int) {
	return fileDescriptor_9898546896accfc3, []int{10}
}

func (m *Error) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Error.Unmarshal(m, b)
}
func (m *Error) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Error.Marshal(b, m, deterministic)
}
func (m *Error) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Error.Merge(m, src)
}
func (m *Error) XXX_Size() int {
	return xxx_messageInfo_Error.Size(m)
}
func (m *Error) XXX_DiscardUnknown() {
	xxx_messageInfo_Error.DiscardUnknown(m)
}

var xxx_messageInfo_Error proto.InternalMessageInfo

func (m *Error) GetErrorCode() int32 {
	if m != nil {
		return m.ErrorCode
	}
	return 0
}

func (m *Error) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func init() {
	proto.RegisterType((*PhoneNumber)(nil), "freya.PhoneNumber")
	proto.RegisterType((*SendSMSParams)(nil), "freya.SendSMSParams")
	proto.RegisterMapType((map[string]string)(nil), "freya.SendSMSParams.ParamsEntry")
	proto.RegisterType((*SendSMSResponse)(nil), "freya.SendSMSResponse")
	proto.RegisterType((*TemplatesList)(nil), "freya.TemplatesList")
	proto.RegisterMapType((map[string]*TemplateFields)(nil), "freya.TemplatesList.TemplatesEntry")
	proto.RegisterType((*TemplateFields)(nil), "freya.TemplateFields")
	proto.RegisterMapType((map[string]string)(nil), "freya.TemplateFields.FieldsEntry")
	proto.RegisterType((*SendEmailParams)(nil), "freya.SendEmailParams")
	proto.RegisterMapType((map[string]string)(nil), "freya.SendEmailParams.ParamsEntry")
	proto.RegisterMapType((map[int32]string)(nil), "freya.SendEmailParams.ToEntry")
	proto.RegisterType((*SendEmailResponse)(nil), "freya.SendEmailResponse")
	proto.RegisterType((*TemplateData)(nil), "freya.TemplateData")
	proto.RegisterType((*SaveTemplateResponse)(nil), "freya.SaveTemplateResponse")
	proto.RegisterType((*UpdateTemplateResponse)(nil), "freya.UpdateTemplateResponse")
	proto.RegisterType((*Error)(nil), "freya.Error")
}

func init() { proto.RegisterFile("freyacon/go/freya.proto", fileDescriptor_9898546896accfc3) }

var fileDescriptor_9898546896accfc3 = []byte{
	// 673 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xa4, 0x55, 0xe1, 0x6e, 0xd3, 0x30,
	0x10, 0x5e, 0xd2, 0xa5, 0x5d, 0x2f, 0xdd, 0x06, 0x47, 0x29, 0x51, 0x06, 0xa8, 0x84, 0x3f, 0x93,
	0x90, 0x52, 0xa9, 0x08, 0x69, 0xdb, 0x9f, 0x69, 0x1a, 0xed, 0x84, 0x80, 0x69, 0x4a, 0xc7, 0x03,
	0xb8, 0xab, 0x57, 0x06, 0x49, 0x1d, 0x25, 0xee, 0x50, 0x5f, 0x00, 0x9e, 0x86, 0x27, 0xe1, 0x1f,
	0x12, 0xef, 0x83, 0x62, 0x3b, 0x69, 0xd2, 0x79, 0x62, 0x68, 0xbf, 0xe2, 0x3b, 0x9f, 0xbf, 0xfb,
	0xee, 0xbb, 0x8b, 0x0d, 0x4f, 0x2e, 0x13, 0xba, 0x20, 0x17, 0x6c, 0xd6, 0x9b, 0xb2, 0x9e, 0x58,
	0xfb, 0x71, 0xc2, 0x38, 0x43, 0x4b, 0x18, 0xee, 0xce, 0x94, 0xb1, 0x69, 0x48, 0x7b, 0xc2, 0x39,
	0x9e, 0x5f, 0xf6, 0x68, 0x14, 0xf3, 0x85, 0x8c, 0xf1, 0x4e, 0xc0, 0x3e, 0xfb, 0xcc, 0x66, 0xf4,
	0x74, 0x1e, 0x8d, 0x69, 0x82, 0x5d, 0xb0, 0x8f, 0xd9, 0x7c, 0xc6, 0x93, 0xc5, 0x31, 0x9b, 0x50,
	0xc7, 0xe8, 0x1a, 0xbb, 0xcd, 0xa0, 0xec, 0xc2, 0x0e, 0xd4, 0x65, 0xac, 0x63, 0x8a, 0x4d, 0x65,
	0x79, 0xbf, 0x0d, 0xd8, 0x1c, 0xd1, 0xd9, 0x64, 0xf4, 0x71, 0x74, 0x46, 0x12, 0x12, 0xa5, 0xb8,
	0x0b, 0x56, 0x9c, 0x41, 0x0b, 0x14, 0xbb, 0x8f, 0xbe, 0xe4, 0x56, 0x4a, 0x17, 0xc8, 0x00, 0xf4,
	0xa0, 0xc5, 0x69, 0x14, 0x87, 0x84, 0xd3, 0x53, 0x12, 0x51, 0xa7, 0x26, 0x90, 0x2b, 0x3e, 0xdc,
	0x83, 0x7a, 0x2c, 0x70, 0x9d, 0xf5, 0x6e, 0x6d, 0xd7, 0xee, 0x77, 0x15, 0x5c, 0x25, 0xa7, 0x2f,
	0x3f, 0x83, 0x8c, 0x6d, 0xa0, 0xe2, 0xdd, 0x7d, 0xb0, 0x4b, 0x6e, 0x7c, 0x00, 0xb5, 0xaf, 0x74,
	0xa1, 0x4a, 0xcb, 0x96, 0xd8, 0x06, 0xeb, 0x9a, 0x84, 0x73, 0xaa, 0x2a, 0x92, 0xc6, 0x81, 0xb9,
	0x67, 0x78, 0xef, 0x60, 0x5b, 0xe1, 0x07, 0x34, 0x8d, 0xd9, 0x2c, 0xa5, 0x88, 0xb0, 0x9e, 0xd2,
	0x19, 0x17, 0xe7, 0x37, 0x02, 0xb1, 0x46, 0x0f, 0x2c, 0x9a, 0x24, 0x4c, 0x4a, 0x62, 0xf7, 0x5b,
	0x8a, 0xda, 0x20, 0xf3, 0x05, 0x72, 0xcb, 0xfb, 0x65, 0xc0, 0xe6, 0xb9, 0x2a, 0x28, 0xfd, 0x70,
	0x95, 0x72, 0x3c, 0x82, 0x66, 0x5e, 0x61, 0xea, 0x18, 0xa2, 0xa8, 0x97, 0xea, 0x64, 0x25, 0x70,
	0x69, 0xc9, 0xba, 0x96, 0xa7, 0xee, 0x92, 0xd8, 0x1d, 0xc1, 0x56, 0x15, 0x40, 0xa3, 0xc0, 0xab,
	0xb2, 0x02, 0x76, 0xff, 0xf1, 0x0a, 0x8d, 0xe1, 0x15, 0x0d, 0x27, 0x69, 0x59, 0x98, 0xef, 0xc6,
	0x12, 0x55, 0xee, 0xe2, 0x3e, 0xd4, 0x2f, 0xc5, 0x4a, 0xd5, 0xf2, 0x42, 0x0b, 0xe2, 0xcb, 0x8f,
	0xea, 0x90, 0x3c, 0x90, 0x75, 0xa8, 0xe4, 0xfe, 0xaf, 0x0e, 0xfd, 0x34, 0x65, 0x8b, 0x06, 0x11,
	0xb9, 0x0a, 0xd5, 0xe0, 0xf9, 0x60, 0x72, 0xa6, 0x58, 0x3c, 0x2f, 0x8d, 0x49, 0x29, 0xc6, 0x3f,
	0x67, 0x92, 0x82, 0xc9, 0x19, 0x3a, 0xd0, 0x48, 0xe7, 0xe3, 0x2f, 0xf4, 0x82, 0x2b, 0xfc, 0xdc,
	0xbc, 0xd3, 0x60, 0x1e, 0xac, 0x0c, 0xa6, 0x77, 0x4b, 0x46, 0xdd, 0x68, 0xbe, 0x81, 0x86, 0x22,
	0x52, 0x2e, 0xda, 0xfa, 0x47, 0xd1, 0xf7, 0x99, 0xe8, 0xf7, 0xf0, 0xb0, 0x20, 0x76, 0xef, 0x99,
	0x1e, 0x42, 0x2b, 0xef, 0xee, 0x5b, 0xc2, 0xc9, 0x0d, 0xb9, 0x0c, 0x8d, 0x5c, 0x08, 0xeb, 0x13,
	0xc2, 0x89, 0x80, 0x6d, 0x05, 0x62, 0xed, 0x71, 0x68, 0x8f, 0xc8, 0x35, 0xcd, 0xb1, 0x0a, 0x5e,
	0x6d, 0xb0, 0x52, 0x72, 0x4d, 0x27, 0x8a, 0x98, 0x34, 0x6e, 0x64, 0x31, 0x35, 0x59, 0x0a, 0xf6,
	0xb5, 0xdb, 0xd9, 0xff, 0x30, 0xa0, 0xf3, 0x29, 0x9e, 0x10, 0x7e, 0x33, 0xb1, 0x03, 0x8d, 0xb9,
	0xd8, 0xc9, 0x53, 0xe7, 0x26, 0xf6, 0x60, 0x23, 0x4f, 0xa4, 0x94, 0x79, 0xb4, 0x32, 0xe7, 0x99,
	0x12, 0x41, 0x11, 0x74, 0x27, 0x26, 0x87, 0x60, 0x09, 0x1b, 0x9f, 0x42, 0x53, 0x78, 0x8a, 0xcb,
	0xd7, 0x0a, 0x96, 0x8e, 0x8c, 0x55, 0x44, 0xd3, 0x94, 0x4c, 0xf3, 0x9a, 0x73, 0xb3, 0xff, 0xc7,
	0x04, 0x6b, 0x98, 0xe1, 0xe2, 0x00, 0xb6, 0x33, 0x29, 0x4f, 0xe9, 0xb7, 0x9c, 0x0f, 0xea, 0x08,
	0xba, 0x3b, 0xf9, 0x94, 0x6a, 0x74, 0xf7, 0xd6, 0x70, 0x08, 0x5b, 0x55, 0x69, 0xf4, 0x28, 0xcf,
	0x94, 0x53, 0x2f, 0xa3, 0xb7, 0x86, 0x47, 0xb0, 0x7d, 0x42, 0xf9, 0x51, 0x18, 0x16, 0x57, 0x10,
	0x76, 0x7c, 0xf9, 0x1e, 0xf9, 0xf9, 0x7b, 0xe4, 0x0f, 0xb2, 0xf7, 0xc8, 0x6d, 0xeb, 0xee, 0x3e,
	0x6f, 0x0d, 0x0f, 0xa1, 0x59, 0x4c, 0x2c, 0x76, 0xf4, 0x3f, 0x97, 0xeb, 0xac, 0xfa, 0x4b, 0x1c,
	0xf6, 0xa1, 0xa1, 0x2e, 0x71, 0x6c, 0xeb, 0x1e, 0x0d, 0xb7, 0x53, 0xf5, 0x2e, 0x8f, 0x8e, 0xeb,
	0x82, 0xe3, 0xeb, 0xbf, 0x01, 0x00, 0x00, 0xff, 0xff, 0xd1, 0x42, 0xce, 0x0c, 0x63, 0x07, 0x00,
	0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// FreyaClient is the client API for Freya service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type FreyaClient interface {
	SaveNewTemplate(ctx context.Context, in *TemplateData, opts ...grpc.CallOption) (*SaveTemplateResponse, error)
	UpdateTemplate(ctx context.Context, in *TemplateData, opts ...grpc.CallOption) (*UpdateTemplateResponse, error)
	GetAllTemplates(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*TemplatesList, error)
	SendEmail(ctx context.Context, in *SendEmailParams, opts ...grpc.CallOption) (*SendEmailResponse, error)
	SendSMS(ctx context.Context, in *SendSMSParams, opts ...grpc.CallOption) (*SendSMSResponse, error)
}

type freyaClient struct {
	cc *grpc.ClientConn
}

func NewFreyaClient(cc *grpc.ClientConn) FreyaClient {
	return &freyaClient{cc}
}

func (c *freyaClient) SaveNewTemplate(ctx context.Context, in *TemplateData, opts ...grpc.CallOption) (*SaveTemplateResponse, error) {
	out := new(SaveTemplateResponse)
	err := c.cc.Invoke(ctx, "/freya.Freya/SaveNewTemplate", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *freyaClient) UpdateTemplate(ctx context.Context, in *TemplateData, opts ...grpc.CallOption) (*UpdateTemplateResponse, error) {
	out := new(UpdateTemplateResponse)
	err := c.cc.Invoke(ctx, "/freya.Freya/UpdateTemplate", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *freyaClient) GetAllTemplates(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*TemplatesList, error) {
	out := new(TemplatesList)
	err := c.cc.Invoke(ctx, "/freya.Freya/GetAllTemplates", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *freyaClient) SendEmail(ctx context.Context, in *SendEmailParams, opts ...grpc.CallOption) (*SendEmailResponse, error) {
	out := new(SendEmailResponse)
	err := c.cc.Invoke(ctx, "/freya.Freya/SendEmail", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *freyaClient) SendSMS(ctx context.Context, in *SendSMSParams, opts ...grpc.CallOption) (*SendSMSResponse, error) {
	out := new(SendSMSResponse)
	err := c.cc.Invoke(ctx, "/freya.Freya/SendSMS", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// FreyaServer is the server API for Freya service.
type FreyaServer interface {
	SaveNewTemplate(context.Context, *TemplateData) (*SaveTemplateResponse, error)
	UpdateTemplate(context.Context, *TemplateData) (*UpdateTemplateResponse, error)
	GetAllTemplates(context.Context, *empty.Empty) (*TemplatesList, error)
	SendEmail(context.Context, *SendEmailParams) (*SendEmailResponse, error)
	SendSMS(context.Context, *SendSMSParams) (*SendSMSResponse, error)
}

func RegisterFreyaServer(s *grpc.Server, srv FreyaServer) {
	s.RegisterService(&_Freya_serviceDesc, srv)
}

func _Freya_SaveNewTemplate_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TemplateData)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FreyaServer).SaveNewTemplate(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/freya.Freya/SaveNewTemplate",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FreyaServer).SaveNewTemplate(ctx, req.(*TemplateData))
	}
	return interceptor(ctx, in, info, handler)
}

func _Freya_UpdateTemplate_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TemplateData)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FreyaServer).UpdateTemplate(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/freya.Freya/UpdateTemplate",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FreyaServer).UpdateTemplate(ctx, req.(*TemplateData))
	}
	return interceptor(ctx, in, info, handler)
}

func _Freya_GetAllTemplates_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(empty.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FreyaServer).GetAllTemplates(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/freya.Freya/GetAllTemplates",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FreyaServer).GetAllTemplates(ctx, req.(*empty.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _Freya_SendEmail_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendEmailParams)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FreyaServer).SendEmail(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/freya.Freya/SendEmail",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FreyaServer).SendEmail(ctx, req.(*SendEmailParams))
	}
	return interceptor(ctx, in, info, handler)
}

func _Freya_SendSMS_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendSMSParams)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FreyaServer).SendSMS(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/freya.Freya/SendSMS",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FreyaServer).SendSMS(ctx, req.(*SendSMSParams))
	}
	return interceptor(ctx, in, info, handler)
}

var _Freya_serviceDesc = grpc.ServiceDesc{
	ServiceName: "freya.Freya",
	HandlerType: (*FreyaServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SaveNewTemplate",
			Handler:    _Freya_SaveNewTemplate_Handler,
		},
		{
			MethodName: "UpdateTemplate",
			Handler:    _Freya_UpdateTemplate_Handler,
		},
		{
			MethodName: "GetAllTemplates",
			Handler:    _Freya_GetAllTemplates_Handler,
		},
		{
			MethodName: "SendEmail",
			Handler:    _Freya_SendEmail_Handler,
		},
		{
			MethodName: "SendSMS",
			Handler:    _Freya_SendSMS_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "freyacon/go/freya.proto",
}
